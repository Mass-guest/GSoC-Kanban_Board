# GSoC - Kanban Board for Debian Bug Tracker and CalDav Server

### What is it ?

Git repository for debian's GSoC project [Kanban Board and CalDav Server](https://wiki.debian.org/SummerOfCode2018/Projects/KanbanBoardForDebianBugTrackerAndCalDAVServer)

## What's needed ?

You'll need :

   - PyQt4: 
```sh
         sudo apt-get install python-qt4
```
   if this fail please follow those instructions http://pyqt.sourceforge.net/Docs/PyQt4/installation.html,

   - CalDav python lib: 
```sh
         pip install caldav
```
   - ICalendar python lib: 
```sh
         pip install icalendar
```

   - Redmine python lib: 
```sh
         pip install python-redmine 
```
   - DateUtil python lib:
```sh
         pip install python-dateutil
``` 

## How to use it ?

```sh
	python controller.py 
```

some test are also disponible with 

```sh
   python view.py
```

## TO DO :

- [x] Add issue gui form (based on the ModifyForm)
- [ ] Github model support
- [ ] Bug Tracker's Source model support
- [x] SVG export support
- [ ] Get possible property for add and modification form from model itself
- [ ] Drag and drop status modification (at model level)
- [ ] ICalendar files (.ics, .ical) model support or/and import option
- [ ] Group subsets of tasks from different sources that need to be visualized on the same board
- [ ] Extend use of source_config.json (ex: add "isComboBox" and "potentialValue" field for object)