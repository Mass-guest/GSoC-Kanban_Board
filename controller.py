from model import *
from view import *

import urllib
import sys
import json

# Dict for all sources config (see class SrcObject)
all_sources = {}

# Replace unicode char to their ascii value
def unicodetoascii(data):
	data = (data.replace('\xe2\x80\x99', "'")
	.replace('\xc3\xa9', 'e')
	.replace('\xe2\x80\x90', '-')
	.replace('\xe2\x80\x91', '-')
	.replace('\xe2\x80\x92', '-')
	.replace('\xe2\x80\x93', '-')
	.replace('\xe2\x80\x94', '-')
	.replace('\xe2\x80\x94', '-')
	.replace('\xe2\x80\x98', "'")
	.replace('\xe2\x80\x9b', "'")
	.replace('\xe2\x80\x9c', '"')
	.replace('\xe2\x80\x9c', '"')
	.replace('\xe2\x80\x9d', '"')
	.replace('\xe2\x80\x9e', '"')
	.replace('\xe2\x80\x9f', '"')
	.replace('\xe2\x80\xa6', '...')
	.replace('\xe2\x80\xb2', "'")
	.replace('\xe2\x80\xb3', "'")
	.replace('\xe2\x80\xb4', "'")
	.replace('\xe2\x80\xb5', "'")
	.replace('\xe2\x80\xb6', "'")
	.replace('\xe2\x80\xb7', "'")
	.replace('\xe2\x81\xba', "+")
	.replace('\xe2\x81\xbb', "-")
	.replace('\xe2\x81\xbc', "=")
	.replace('\xe2\x81\xbd', "(")
	.replace('\xe2\x81\xbe', ")"))

	return data

# Verify if a filename is valid based on different extensions
def filename_is_ok(name, extensions):
	return name == name.rsplit('.', 1)[0] or name.rsplit('.', 1)[1].lower() not in extensions

# Convert an icalObject to an html string
def ical_to_html(icalObj):
	listStatus = []
	todo_list = []
	
	#TODO : add css and maybe some js here

	#Make a list of issue's status 
	#and a list of list of those issue
	for todo in icalObj.walk("vtodo"):
		state = todo.get("status", None)

		if state == None :
			continue

		if state not in listStatus:
			todo_list.append(list())
			listStatus.append(state)

		i = listStatus.index(state)
		todo_list[i].append(str(todo["summary"]))

	#Get max size of all list in todo_list
	size = len(max(todo_list, key = lambda l: len(l)))

	#Writre table header with status name
	table_html = "<table><tr>"
	for status in listStatus:
		table_html += "<th>"
		table_html += status
		table_html += "</th>"
	table_html += "</tr>"

	#Sorted all list in todo_list
	for j in range(len(todo_list)):
		todo_list[j] = sorted(todo_list[j])

	#Write html table
	for i in range(size):
		table_html += "<tr>"
		for j in range(len(todo_list)):
			table_html += "<td>"
			if len(todo_list[j]) > i :
				todo_list[j][i] = unicodetoascii(todo_list[j][i])
				table_html += "<div id='card'>"+todo_list[j][i].replace("<","&lt;").replace(">","&gt;")+"</div>"
				continue
			table_html += "</td>"
		table_html += "</tr>"
	table_html += "</table>"
	return table_html

# Convert an status card list to an svg string
def ical_to_svg(status_list, status_names):
	bottom_margin = 10
	right_margin = 10
	
	# Total width and height of the svg
	width = sum([lst.maxWidth for lst in status_list]) + len(status_list)*right_margin
	height = max([sum([lst.rowHeight(i)+bottom_margin for i in range(lst.rowCount())]) for lst in status_list])

	svg_header = "<!DOCTYPE html><html><body><svg width='"+str(width)+"' height='"+str(height)+"'>"

	svg_content = ""
	
	padding_x = 15
	current_x = 0
	# Writing status name text
	for i in range(len(status_names)):
		lst = status_list[i]
		width = lst.maxWidth
		svg_content += "<foreignObject x='"+str(current_x+5)+"' y='00' width='"+str(width)+"' height='50'>"
		svg_content += "<p style='font-size:14px' xmlns='http://www.w3.org/1999/xhtml'>"+status_names[i]+"</p>"
		svg_content += "</foreignObject>"
		current_x += width+right_margin

	current_x = 0
	# Drawing color rectangle with text
	for lst in status_list :
		width = lst.maxWidth
		current_y = 40
		for r in range(lst.rowCount()):
			height = lst.rowHeight(r)

			widget = lst.cellWidget(r, 0)

			color = "rgb("+str(widget.color.red())+","+str(widget.color.green())+","+str(widget.color.blue())+")"

			svg_content += "<rect x='"+str(current_x)+"' y='"+str(current_y)+"' fill='"+color+"' width='"+str(width)+"' height='"+str(height)+"'/>"
			# TODO : find a way to wrap text with text tag instead of foreignObject
			svg_content += "<foreignObject x='"+str(current_x+padding_x)+"' y='"+str(current_y+5)+"' width='"+str(width-padding_x*2)+"' height='"+str(height)+"'>"
			svg_content += "<p style='font-size:12px' xmlns='http://www.w3.org/1999/xhtml'>"+widget.name+"</p>"
			svg_content += "</foreignObject>"
			current_y += height + bottom_margin
		current_x += width+right_margin

	svg_footer = "</svg></body></html>"
	
	return svg_header + svg_content + svg_footer



class SrcObject(object):
	"""Container object to set link data needed by view and provided by model"""
	# SUGGESTION : replace by simple dict


	def __init__(self, modelClass, loginDict, getDict, postDict):
		super(SrcObject, self).__init__()
		self.modelClass = modelClass
		self.loginDict = loginDict
		self.getDict = getDict
		self.postDict = postDict

# Convert a pair of key,value to a qt html string 
def to_qt_html(key, value):
	#List of key that we don't want to be on the description
	indesirable = ["PRIORITY", "UID"]
	if key in indesirable :
		return ""

	#URL
	if key == "URL" :
		return "<a href=\""+urllib.unquote(str(value))+"\">"+str(key)+"</a>"
	#Dates
	if type(value) == vDDDTypes :
		return "<qt>"+str(key).capitalize().replace("-"," ")+" = "+str(value.dt).replace("<","&lt;").replace(">","&gt;")+"</qt>"""
	#X types (bonus arguments)
	if key[0] == "X" :
		key = key[2:]
	#Others
	return "<qt>"+str(key).capitalize().replace("-"," ")+" = "+str(value).replace("<","&lt;").replace(">","&gt;")+"</qt>"""



class Controller(object):
	"""Controller class that make link between model and view, it's the main class"""


	def __init__(self):
		super(Controller, self).__init__()
		reload(sys)  
		# TODO : find another way to handle latin-1 and utf8 unicode displaying
		sys.setdefaultencoding('utf8')

		self.app = QApplication(sys.argv)
		self.model = None
		self.view = None
		self.cal = None
		self.style_dict = {}
		self.load_config()


	def start(self):
		self.switch_view(LoginView(self, all_sources))


	def quit(self):
		sys.exit(self.app.exec_())

	# Initialise all model source object to make link between view and model through controller
	def load_config(self):
		configFile = open("source_config.json", "r")
		configDict = json.load(configFile)

		configFile.close()

		for source_name, source_config in configDict.items():
			#Get class objects of config given
			ModelClass = globals()[source_config["model"]]

			for key in source_config["post"] :
				source_config["post"][key] = globals()[source_config["post"][key]]

			#Init new SrcObject
			all_sources[source_name] = SrcObject(ModelClass, source_config["login"], source_config["get"], source_config["post"])

	# Init model with args given by user and switch to GetView to get calendar
	def load_model(self, args):
		try:
			#Get SrcObject corresponding args
			self.modelConfig = all_sources[args["model_type"]]
			#Init model
			ModelClass = self.modelConfig.modelClass
			self.model = ModelClass(args)
			#Open get calendar view
			self.switch_view(GetView(self, self.modelConfig.getDict))
		except Exception as e :
			#TODO : adapt msg by exception

			#Display exception message on view
			print('ERROR : '+str(e))
			self.view.display_error(e)
			

	# Launch main calendar view from args given by user if args are valid
	def get_calendar(self, args):
		try:
			self.cal = self.model.Get(args)
			self.switch_view(MainView(self))

			# make a list of todos to add them in a sorted way
			# make a set of status at same time
			status_names = set()
			todos = []
			for todo in self.cal.walk('vtodo') :
				state = todo.get("status", None)
				if state != None :
					status_names.add(str(state))
					todos.append(todo)

			# add all todo on view
			for todo in sorted(todos, key=lambda t : t.decoded("summary").lower()) :
				todo["summary"] = unicodetoascii(todo["summary"])
				self.view.add_Card(todo)
		except Exception as e:
			#TODO : adapt msg by exception

			#Display exception message on view
			print('ERROR : '+str(e))
			self.view.display_error(e)

	# Function called when user click on an issue Card or issue name in the list
	# Display issue infos and select it on the view
	def current_todo_change(self, name):
		#TODO : make selection change done by uid instead of name
		for todo in self.cal.walk('vtodo') :
			if todo["summary"] == name :
				break
				
		txt = ""
		for key, value in sorted(todo.items()):
			txt += to_qt_html(key, value)

		#Accept url link
		self.view.todo_info.setOpenExternalLinks(True)

		self.view.todo_info.setText(txt)

		#Ask board to select this todo
		self.view.board.select_todo(name.__str__())

	# Return a function that change view style to given style (by name)
	def change_style(self, new_style):
		def wrap() :
			style = QStyleFactory.create(new_style)
			self.app.setPalette(style.standardPalette())
			self.app.setStyle(style)
		return wrap

	# Switch main view
	def switch_view(self, new_view):
		if self.view != None :
			self.view.deleteLater()
			self.view.hide()
		self.view = new_view
		self.view.show()

	# TODO : reload the model and view
	def reload_(self):
		print("Reload not done yet")

	# Open popup to choose a filename
	def choose_file(self, caption, filter):
		return str(QFileDialog.getSaveFileName(caption=caption, filter=filter))
	
	# Export board to ical file
	def export_to_ical(self):
		file = self.choose_file("ICalendar file", "ICalendar file (*.ical *.ics)")
		if file == "":
			return

		if filename_is_ok(file,("ical", "ics")) :
			file += ".ical"

		with open(file,"w+") as file:
			file.write(self.cal.to_ical())

	# Export board to ical file
	def export_to_html(self):
		file = self.choose_file("HTML file", "HTML file (*.html)")
		if file == "":
			return
		if filename_is_ok(file,("html")) :
			file += ".html"

		with open(file, "w+") as file :
			file.write(ical_to_html(self.cal))
		
	# TODO : Export board to svg file
	def export_to_svg(self):
		file = self.choose_file("SVG file", "SVG file (*.svg)")
		if file == "":
			return
		if filename_is_ok(file,("svg")) :
			file += ".svg"

		with open(file, "w+") as file :
			file.write(ical_to_svg(self.view.board.status_object, self.view.board.status_names))


	# Setup issue modification based on uid
	def modify_todo_launch(self, uid):
		if not self.model.support_Put :
			raise UnsupportedException()
		
		for todo in self.cal.walk("vtodo") :
			if todo["uid"] == uid :
				break
		if todo["uid"] != uid:
			raise Exception("UID given isn't in calendar, should never happend")

		#TODO : add info needed by model from source_config.json
		self.view.openModifyForm(todo, self) 

	# Modify todo, called when accept "Modify" on modification form
	def modify_todo_content(self):
		args = self.view.modifyForm.getArgs()
		modified_todo = self.view.modifyForm.todo

		old_name = modified_todo["summary"]

		#Replace all todo arguments by new ones
		for key in args :
			modified_todo[key] = args[key]

		#Priority need to be an int
		modified_todo["priority"] = int(modified_todo["priority"])
		
		try :
			if not self.model.Put({"todo":modified_todo}) :
				raise Exception("An error occured")
		except Exception as e:
			self.view.modifyForm.display_error(e)
			return

		#Load changement in the view
		if old_name != modified_todo["summary"]:
			self.view.replace_todo_name(old_name, modified_todo["summary"])

		self.view.board.update_todo(modified_todo)

		#Replace todo in calendar
		for todo in self.cal.walk("vtodo"):
			if todo["uid"] == modified_todo["uid"]:
				for key in modified_todo:
					todo[key] = modified_todo[key]
				break

		#Select this todo
		self.current_todo_change(modified_todo["summary"])

		#Close and delete modify window
		self.view.modifyForm.deleteLater()
		self.view.modifyForm.hide()


	# Setup issue creation
	def add_todo_launch(self):
		if not self.model.support_Post :
			raise UnsupportedException()

		#TODO : add info needed by model from source_config.json
		self.view.openAddForm(self.modelConfig.postDict, self) 

	# Add new todo, called when accept "Add Card" on add form
	def add_todo_content(self):
		args = self.view.addForm.getArgs()

		new_todo = Todo()
		new_todo['summary'] = args['summary']
		new_todo['priority'] = int(args['priority'])
		new_todo['status'] = args['status']

		try :
			new_todo = self.model.Post({"todo":new_todo})
			if not new_todo :
				raise Exception("An error occured")
		except Exception as e:
			self.view.addForm.display_error(e)
			return

		#Add in ical
		self.cal.add_component(new_todo)

		#Load changement in the view
		self.view.add_Card(new_todo)

		#Select this todo
		self.current_todo_change(new_todo["summary"])

		#Close and delete modify window
		self.view.addForm.deleteLater()
		self.view.addForm.hide()

	# Display a popup to confirm close demand
	def close_todo(self, uid):
		if not self.model.support_Close :
			raise UnsupportedException()
	
		for todo in self.cal.walk("vtodo") :
			if todo["uid"] == uid :
				break
		if todo["uid"] != uid:
			raise Exception("UID given isn't in calendar, should never happend")

		if not self.view.checkMessage("Are you sure, that you want to close this issue ?") :
			return False

		return self.model.Close({"todo":todo})

	# Display a popup with msg argument as text
	def inform(self, msg):
		self.view.inform(msg)


def main():
	controller = Controller()
	controller.start()
	sys.exit(controller.app.exec_())

if __name__ == '__main__':
	main()