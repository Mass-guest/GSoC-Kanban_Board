# Model for Kanban Board MVC
import sys

from icalendar import *

import caldav
import caldav.elements.dav as DAV

from redminelib import Redmine

import requests
import datetime

from itertools import takewhile
from dateutil.parser import parse as date_parse

if sys.version_info[0] < 3:
	import urllib2 as url_lib
else:
	import urllib.request as url_lib


class UnsupportedException(Exception):
	

	def __init__(self):
		super(UnsupportedException, self).__init__("This source doesn't support this action yet.")


# Parent API Module
class SourceObject(object):
	"""Parent of all source object"""
	def __init__(self, args):
		pass
	

	def Get(self, args):
		"""Get ICalendar object from the source"""
		raise UnsupportedException()
		pass

	support_Post = False
	def Post(self, args):
		"""Post new task object from the source"""
		raise UnsupportedException()
		pass

	support_Put = False
	def Put(self, args):
		"""Update task object from the source"""
		raise UnsupportedException()
		pass

	support_Close = False
	def Close(self, args):
		"""Close task object from the source"""
		raise UnsupportedException()
		pass


class CalDavSource(SourceObject):
	"""CalDav object for API Request"""
	def __init__(self, args):
		url = args.get("url", None)
		username = args.get("username", None)
		password = args.get("password", None)
		super(CalDavSource, self).__init__(args)
		client = caldav.DAVClient(url, username=username, password=password)
		self.principal = client.principal()

	""" params :
			- args : dictionnary of args needed by the source to get a calendar
					 CalDav need calendar's display name
		return :
			icalendar.Calendar object if calendar with displayname == name exist
			None otherwise
	"""
	def Get(self, args):
		name = args.get("name", None)

		self.davcal = None
		for cal in self.principal.calendars():
			properties = cal.get_properties([DAV.DisplayName(), ])
			display_name = properties['{DAV:}displayname']
			if display_name == name:
				self.davcal = cal
				break

		# if no calendar with this name
		if self.davcal == None:
			raise ValueError("Calendar named '" + name + "' not found")

		cal = Calendar()
		for todo in self.davcal.todos():
			cal.add_component(Todo.from_ical(todo.data))
		return cal

	""" params :
			- args : todo (icalendar.Todo) object to Post
		return :
			url of ics posted
		throw :
			Exception
	"""
	support_Post = True
	def Post(self, args):
		"""Post new task object from the source"""
		todo = args.get("todo", None)
		begin = "BEGIN:VCALENDAR\r\n"
		end = "END:VCALENDAR"
		result = self.davcal.add_todo(begin + todo.to_ical() + end)

		uid = str(result).split('/')[-1].replace('.ics','')
		todo['uid'] = uid
		return todo

	""" params :
			- args :  todo (icalendar.Todo) object to Put
		return :
			url of ics put
		throw :
			Exception
	"""
	support_Put = True
	def Put(self, args):
		"""Update task object from the source"""
		return self.Post(args)

	""" params :
			- args : todo (icalendar.Todo) object to Close
		return :
			True if done, False otherwise
		throw :
			Exception
	"""
	support_Close = True
	def Close(self, args):
		args["todo"]["status"] = "COMPLETED"
		
		return self.Post(args)
		

# TODO : create a similar source for icalendar file in local storage
class ICalendarSource(SourceObject):
	"""ICalendar object for API Request"""


	def __init__(self, args):
		super(ICalendarSource, self).__init__(args)

	""" params : 
				- args : dictionnary of args needed by the source to get a calendar
					 ICalendar need the url of the ics file
		return :
			icalendar.Calendar object if a calendar is in path
			None otherwise
	"""
	def Get(self, args):
		self.url = args.get("url", None)
		data = url_lib.urlopen(self.url).read()
		cal = Calendar.from_ical(data)
		return cal


class BugZillaSource(ICalendarSource):
	"""BugZilla object for API Request"""
	
	

	def __init__(self, args):
		#TODO : add api_key support and auth check
		self.api_key = args.get("api_key")
		self.is_set_priority_tab = False
		self.priority_tab = ["Highest","High","Normal","Low","Lowest","---"]
		super(BugZillaSource, self).__init__(args)

	# Priority can change from bugzilla implementation ... mozilla have P1, P2...etc, need setpriority before use
	# Before Post we need to set a table of possible priority 
	def setPriorityTab(self):
		if (self.is_set_priority_tab):
			return
		p_tab = []

		url = self.url.split("/buglist.cgi", 1)[0]+"/rest.cgi/field/bug/priority/values"

		r = requests.get(url)

		values = list(reversed([v.encode("utf8") for v in r.json()["values"]]))

		self.priority_tab = values
	

	""" params :
			- args : todo (icalendar.Todo) to post
		return :
			boolean : True if success, False otherwise
	"""
	support_Post = True
	def Post(self, args):
		self.setPriorityTab()
		todo = args.get("todo", None)
		requests_params = {"summary": todo["summary"],
		"product": args.get("product", None),
		"component": args.get("component", None),
		"version": args.get("version", None),
		"op_sys":"All",
		"rep_platform":"All",
		"priority": self.priority_tab[todo["priority"]-1],
		"api_key": self.api_key}

		url = self.url.split("/buglist.cgi", 1)[0]+"/rest.cgi/bug"

		r = requests.post(url, params=requests_params)
		"""TODO : Post need to return a result as the new icalendar.Todo
		          need to recuperate the new uid"""
		print(r.json())
		if not r.ok :
			if r.json().get("error", False):
				raise Exception(r.json()['message'])
			return None
		if not r.json().get("error", False) :
			for key, value in args.items():
				todo['X-'+key.replace(' ','-')] = value
			
			# TODO : set uid
			return todo
		raise Exception(r.json()['message'])
		


class BTSSource(ICalendarSource):
	"""BTS (Bug Tracker's Source) object for API Request"""
	def __init__(self, args):
		super(BTSSource, self).__init__(args)
	# TODO : Find BTS icalendar feed
	# TODO : Post/Put send an email


class RedmineSource(SourceObject):
	"""Redmine object for API Request"""

	""" dictionnary that is used to convert redmine issue 
		to icalendar vtodo through lambda  """
	equivalence = {
		"subject": ("SUMMARY", (lambda self, x: x)),
		"id": ("UID", (lambda self, x: str(x))),
		"priority": ("PRIORITY", (lambda self, x: x)),  # priority
		"start_date": ("DTSTART", (lambda self, x: date_parse(str(x)))),
		"status": ("STATUS", (lambda self, x: x)),  # status
		"updated_on": ("LAST-MODIFIED", (lambda self, x: date_parse(str(x))))
		#TODO : add bonus arguments, example : "bonus_arg" : ("X-Bonus-Arg",(lambda self, x: x))
	}

	# dictionnary of status_id by name for POST/PUT methods
	status_id_dict = None
	# status that represent closed state 
	closed_status = None
	
	# init status dictionnary from redmine client
	def init_status(self):
		statuses = self.client.issue_status.all()

		self.closed_status = ""

		self.status_id_dict = {}

		for elem in statuses.values() :
			stat = elem
			self.status_id_dict[stat['name']] = stat['id']
			if stat.get('is_closed', False) :
				self.closed_status = stat['name']


	""" params :
			- args : url of redmine source, (user's username and password) or (api key) 
		return :
			nothing
		throw :
			Authentication error if user's info are wrong
	"""
	def __init__(self, args):
		super(RedmineSource, self).__init__(args)
		username = args.get("username", None)
		password = args.get("password", None)
		self.url = args.get("url", None)

		try:
			self.client = Redmine(
				self.url, username = username, password = password)
			self.client.auth()

		except Exception as e:
			api_key = args.get("api_key", None)
			self.client = Redmine(self.url, key = api_key)
			self.client.auth()

		self.init_status()


	""" params :
			- args : redmine Issue object
		return :
			equivalent icalendar.Todo object 
	"""
	def issue_to_todo(self, issue):
		todo = Todo()

		for key in dir(issue):
			if (RedmineSource.equivalence.get(key, None) == None) :
				continue

			VName, eq_func = RedmineSource.equivalence[key]
			VValue = eq_func(self, issue[key])

			todo.add(VName, VValue)

		return todo

	""" params :
			- args : dictionnary of args needed by the source to get a calendar
					 Redmine API need the identifier of the project
		return :
			icalendar.Calendar object if a calendar is in path
			None otherwise
		throw :
			ResourceNotFoundError there is no project with this identifier
	"""
	def Get(self, args):
		self.identifier = args.get("identifier", None)

		self.project = self.client.project.get(self.identifier)

		cal = Calendar()
		todo = None

		for issue in self.project.issues:
			if str(issue.status) != self.closed_status :
				todo = self.issue_to_todo(issue)
				cal.add_component(todo)
				todo = None

		return cal

	""" params :
			- args : todo (icalendar.Todo) to post
		return :
			the new icalendar.Todo object
		throw :
			Exception
	"""
	support_Post = True
	def Post(self, args):
		"""Post new task object from the source"""
		ical_todo = args.get("todo", None)
		args = {
			'project_id' : self.identifier,
			'subject' : ical_todo["summary"],
			'status_id' : self.status_id_dict[unicode(ical_todo["status"])]
		}
		issue = self.client.issue.create(**args)

		ical_todo = self.issue_to_todo(issue)

		return ical_todo

	""" params :
			- args : todo (icalendar.Todo) to update
		return :
			True if done, False otherwise
		throw :
			Forbidden Error if user don't have right to update
	"""
	support_Put = True
	def Put(self, args):
		"""Update task object from the source"""
		ical_todo = args.get("todo", None)
		issue_id = int(ical_todo["uid"])
		args = {
		  'subject' : ical_todo["summary"],
		  'status_id' : self.status_id_dict[unicode(ical_todo["status"])],
		  'priority_id' : ical_todo["priority"]
		}

		if (ical_todo.get("dtstart", False)):
			args['start_date'] = ical_todo["dtstart"].dt.date()
		
		result = self.client.issue.update(issue_id, **args)
		return result

	""" params :
			- args : todo (icalendar.Todo) to close
		return :
			True if done, False otherwise
		throw :
			Forbidden Error if user don't have right to update
	"""
	support_Close = True
	def Close(self, args):
		ical_todo = args.get("todo", None)
		issue_id = int(ical_todo["uid"])
		args = {'status_id' : self.status_id_dict[self.closed_status]}
		result = self.client.issue.update(issue_id, **args)

		return result


# TODO : GItHub support
class GitHubSource(SourceObject):
	"""GitHub object for API Request"""
	def __init__(self, args):
		super(GitHubSource, self).__init__(args)
