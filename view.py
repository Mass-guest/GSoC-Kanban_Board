from model import *

import sys

from PyQt4.QtCore import *
from PyQt4.QtGui import *

#TODO : display color chart somewhere
#priority 1 = PRIORITY_COLORS[0] , 2 = PRIORITY_COLORS[1] ...etc
PRIORITY_COLORS = [QColor(141, 14, 4), QColor(192, 68, 31), QColor(195, 155, 5), QColor(13, 98, 56), QColor(102, 255, 102), QColor(4, 40, 88)]



class ValueWidget(QWidget):
	"""Widget containing login form (only form not combo box for source or other) """

	def __init__(self, all_sources):
		super(ValueWidget, self).__init__()
		self.all_sources = all_sources
		self.model_type = None
		self.layout = QFormLayout()
		self.begin_index = 0
		self.setLayout(self.layout)


	#remove all fields from form
	def clear_layout(self):
		self.begin_index = self.layout.rowCount()
		for i in reversed(range(self.layout.count())): 
			self.layout.itemAt(i).widget().setParent(None)


	#load another form according to model type in argument
	def make_form(self, model_type):
		self.clear_layout()
		self.model_type = model_type
		loginDict = self.all_sources[model_type].loginDict
		for label, widget in loginDict.items() :
			w = globals()[widget]()
			self.layout.addRow(label, w)


	#return dict of arguments from form fields as in {field name : field text} 
	def get_args(self):
		args = {"model_type":self.model_type}
		for row in range(self.begin_index, self.layout.rowCount()):
			key = str(self.layout.itemAt(row, 0).widget().text())
			value = str(self.layout.itemAt(row, 1).widget().text())
			args[key] = value
		return args		



class LoginView(QWidget):
	"""Login view, at launch of application to chose source and complete form"""


	def __init__(self, controller, all_sources):
		super(LoginView, self).__init__()
		self.controller = controller
		self.textError = QLabel("")

		#TODO : need to be more flexible
		self.resize(400, 100)

		self.value_widget = ValueWidget(all_sources)

		# init combo box of sources
		self.combo_model = QComboBox()
		
		for model_name in sorted(all_sources):
			self.combo_model.addItem(model_name)

		self.combo_model.currentIndexChanged.connect(self.selection_change)
		#TODO : finder better way to set to first index
		self.combo_model.setCurrentIndex(1)
		self.combo_model.setCurrentIndex(0)

		# init horizontal layout to put launch and quit button
		launchBut = QPushButton("Launch Board")
		launchBut.clicked.connect(self.launch_board)
		quitBut = QPushButton("Quit")
		quitBut.clicked.connect(self.controller.quit)

		hbox = QHBoxLayout()
		hbox.addWidget(launchBut)
		hbox.addStretch()
		hbox.addWidget(quitBut)

		# wrap all component into vertical layout and set the view to it
		vbox = QVBoxLayout()
		vbox.addWidget(self.textError)
		vbox.addWidget(self.combo_model)
		vbox.addStretch()
		vbox.addWidget(self.value_widget)
		vbox.addStretch()
		vbox.addLayout(hbox)

		self.setLayout(vbox)

		self.setWindowTitle("Kanban Board Connection")


	#called when chosen source change
	def selection_change(self, i):
		model_type = self.combo_model.itemText(i)
		self.value_widget.make_form(str(model_type))


	def display_error(self, exception):
		self.textError.setText("<font color='red'>" + str(exception) + "</font>")


	def launch_board(self):
		self.display_error("")

		args = self.value_widget.get_args()

		self.controller.load_model(args)



class GetView(QWidget):
	"""View to choose wich Calendar you'll need from choosen model"""
	def __init__(self, controller, getDict):
		super(GetView, self).__init__()
		self.controller = controller
		self.textError = QLabel("")

		#TODO : need to be more flexible
		self.resize(400, 100)

		# init a much little value widget (no need to handle combo box change)
		self.layout = QFormLayout()

		for label, widget in getDict.items() :
			self.layout.addRow(label.encode("utf8"), globals()[widget]())

		# init launch and quit button to an horizontal layout
		launchBut = QPushButton("Get Calendar")
		launchBut.clicked.connect(self.get_calendar)
		quitBut = QPushButton("Quit")
		quitBut.clicked.connect(self.controller.quit)

		hbox = QHBoxLayout()
		hbox.addWidget(launchBut)
		hbox.addWidget(quitBut)

		# wrap all component into vertical layout and set the view to it
		vbox = QVBoxLayout()
		vbox.addWidget(self.textError)
		vbox.addLayout(self.layout)
		vbox.addLayout(hbox)

		self.setLayout(vbox)

		self.setWindowTitle("Kanban Board Choose Calendar")

	# Display exception error msg in red above form
	def display_error(self, exception):
		#TODO : Display different things for any exception
		self.textError.setText("<font color='red'>" + str(exception) + "</font>")
		self.updateGeometry()

	# Get args from fields (as in ValueWidget)
	# Ask controller to launch calendar with choosen args
	def get_calendar(self):
		self.textError.setText("")
		#TODO : Factorise this with code from ValueWidget get_args method
		args = {}
		for row in range(self.layout.rowCount()):
			key = str(self.layout.itemAt(row, 0).widget().text())
			value = str(self.layout.itemAt(row, 1).widget().text())
			args[key] = value
		
		self.controller.get_calendar(args)



class MainView(QMainWindow):
	"""Main window of the application, contain a calendar, a menu bar, a todo list 
	   and a description field of selected task"""
	def __init__(self, controller, parent = None):
		super(MainView, self).__init__(parent)
		self.resize(1000, 600)

		#Set main widget : calendar 
		self.board = BoardWidget(controller)
		self.board.todo_clicked.connect(controller.current_todo_change)

		#Set widget : todo list
		self.todo_list = QListWidget(self)
		self.todo_list.currentTextChanged.connect(controller.current_todo_change)
		self.todo_list.setSortingEnabled(True)

		#Set widget : todo description 
		self.todo_info = QLabel("No task selected")
		self.todo_info.setWordWrap(True)

		#Set menu bar (File, Export as, Help ...etc)
		self.init_bar(controller)

		#Add dock widget : todo descritpion
		infos = QDockWidget("Description", self)
		infos.setWidget(self.todo_info)
		infos.setFloating(False)
		self.addDockWidget(Qt.BottomDockWidgetArea, infos)

		#Add dock widget : todo list
		todo_dock = QDockWidget("Task List", self)
		todo_dock.setWidget(self.todo_list)
		todo_dock.setFloating(False)
		self.addDockWidget(Qt.RightDockWidgetArea, todo_dock)

		#Add main widget : calendar
		scroll_area = QScrollArea()
		scroll_area.setWidget(self.board)
		scroll_area.setWidgetResizable(True)
		self.setCentralWidget(scroll_area)

		self.setWindowTitle("Kanban Board")

	# Add issue card to window
	def add_Card(self, todo):
		# add Card to board
		self.board.add_Card(todo)

		# add Card to todo list
		self.todo_list.addItem(todo["summary"])
	
	#Initialise the menuBar
	def init_bar(self, controller):
		bar = self.menuBar()
		self.init_file(bar, controller)
		self.init_export(bar, controller)
		self.init_preferences(bar, controller)

	#Initialise file menu on the menuBar
	def init_file(self, bar, controller):
		file = bar.addMenu("File")

		"""
		TODO : reloading option on board
		reload_ = file.addAction("Reload board")
		reload_.triggered.connect(controller.reload_)"""

		create = file.addAction("Create issue")
		create.triggered.connect(self.board.addTodo_launch)

		quit = file.addAction("Quit")
		quit.triggered.connect(controller.quit)

	#Initialise export menu on the menuBar
	def init_export(self, bar, controller):
		export = bar.addMenu("Export as ...")
		
		ical = export.addAction("... ICalendar")
		ical.triggered.connect(controller.export_to_ical)

		html = export.addAction("... HTML")
		html.triggered.connect(controller.export_to_html)

		svg = export.addAction("... SVG")
		svg.triggered.connect(controller.export_to_svg)

	#Initialise file menu on the menuBar
	def init_preferences(self, bar, controller):
		preferences = bar.addMenu("Preferences")

		styles = preferences.addMenu("Styles")
		self.init_style_choice(styles, controller)

	#Initialise preference style choices
	def init_style_choice(self, style_menu, controller):
		ag = QActionGroup(style_menu, exclusive=True)

		for style in QStyleFactory.keys() :
			action = ag.addAction(QAction(style, style_menu, checkable=True))
			style_menu.addAction(action)
			style_func = controller.change_style(style)
			action.triggered.connect(style_func)

		#TODO : load last chosen style instead of last key found
		style_func()
		action.setChecked(True)

	#Open popup modification form
	def openModifyForm(self, todo, controller):
		#TODO : use a list of possible status given by the model
		possible_status = self.board.status_names
		self.modifyForm = ModifyForm(possible_status, todo, controller)
		self.modifyForm.show()

	#Open popup modification form
	def openAddForm(self, modelConfig, controller):
		#TODO : use a list of possible status given by the model
		possible_status = self.board.status_names
		self.addForm = AddForm(possible_status, modelConfig, controller)
		self.addForm.show()

	#Open popup message
	def inform(self, text):
		msg = QMessageBox()
		msg.setWindowTitle("Information")
		msg.setIcon(QMessageBox.Information)
		msg.setText(text)
		msg.setStandardButtons(QMessageBox.Ok)
		msg.exec_()

	#Open popup confirmation message that return if ok or reject confirmation
	def checkMessage(self, msg):
		# create popup and set title
		dialog = QDialog(self)
		dialog.setWindowTitle("Confirmation")

		# create popup layout and add message
		vlayout = QVBoxLayout()
		vlayout.addWidget(QLabel(msg))

		# create ok and cancel button, link it to value returned
		ok_button = QPushButton("OK")
		ok_button.clicked.connect(dialog.accept)
		cancel_button = QPushButton("Cancel")
		cancel_button.clicked.connect(dialog.reject)

		# create button horizontal layout and add buttons 
		hlayout = QHBoxLayout()
		hlayout.addWidget(ok_button)
		hlayout.addWidget(cancel_button)
		
		# add button layout to dialog layout
		vlayout.addLayout(hlayout)

		dialog.setLayout(vlayout)

		dialog.setModal(True)
		
		return dialog.exec_()

	# Replace todo name from old to new (modification purpose)
	def replace_todo_name(self, old, new):
		#replace old text to new text in list
		item = self.todo_list.findItems(old, Qt.MatchFixedString)
		if item :
			item[0].setText(new)

		#replace in board dict
		self.board.replace_todo_name(old, new)



class BoardWidget(QWidget):
	"""Scroll area for todo list container"""
	
	todo_clicked = pyqtSignal(str)

	def __init__(self, controller):
		super(BoardWidget, self).__init__()
		self.todo_dict = dict()
		self.controller = controller

		self.hlayout = QHBoxLayout()
		self.hlayout.setAlignment(Qt.AlignLeft)
		self.setLayout(self.hlayout)

		self.status_object = []
		self.status_names = []

	# Add new status list
	def add_status(self, status):
		if status in self.status_names:
			return
		status_list = CardList(self, status, self.controller)
		
		self.status_object.append(status_list)
		self.status_names.append(status)

		status_list.setColumnCount(1)
		status_list.setHorizontalHeaderLabels([status])

		self.hlayout.addWidget(status_list)

	# Add new card to her status list
	def add_Card(self, todo):
		t = CardWidget(todo, self)
		
		status_name = todo.get("status", None)
		if status_name == None:
			raise Exception("Undefined status")
			return

		self.add_status(status_name)
		
		t_index = self.status_names.index(status_name)
		
		sl = self.status_object[t_index]
		
		sl.insertRow(sl.rowCount())
		
		sl.setCellWidget(sl.rowCount() - 1, 0, t)
		
		self.todo_dict[str(todo["summary"])] = t
		t.show()

	# Replace issue card name in dict
	def replace_todo_name(self, old, new):
		self.todo_dict[new] = self.todo_dict[old] 
		del self.todo_dict[old]

	# Update issue
	def update_todo(self, todo):
		widget = self.todo_dict[todo["summary"]]
		widget.update(todo)
		# TODO : switch status if needed
	
	# Select an issue card (issue list select purpose)
	def select_todo(self, name):
		todo = self.todo_dict[name]
		todo.select()
		todo.list.scrollToCard(todo)

	# Clear selection of other card list than one in argument
	def clearSelectionOther(self, selected):
		for status in self.status_object:
			if status != selected :
				status.clearSelection()
				status.setCurrentCell(-1,-1)

	# Move an issue widget to a status list
	def changeTodoStatus(self, widget, destination_status, drop_row = 0):
		self.todo_dict[widget.name] = widget
		destination_status.setCellWidget(drop_row, 0, widget)
		widget.select()
		
	# Launch add of an issue
	def addTodo_launch(self):
		try :
			self.controller.add_todo_launch()
		except Exception as e :
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText(str(e))
			msg.setStandardButtons(QMessageBox.Ok)
			msg.exec_()



class CardList(QTableWidget):
	"""Object that contain issue from single status"""

	def __init__(self, parent, id_, controller, *args, **kwargs):
		super(CardList, self).__init__(*args, **kwargs)
		self.controller = controller

		self.setDragEnabled(True)
		self.setAcceptDrops(True)
		self.setSelectionBehavior(QAbstractItemView.SelectRows)
		self.setDragDropOverwriteMode(False)
		self.setSelectionMode(QAbstractItemView.SingleSelection)

		self.setStyleSheet(""" 
				QTableView{
							border:none;
							background-color:rgba(0, 0, 0, 0);
							selection-background-color:rgba(150, 150, 150, 0);
							}

				QTableView:hover{
							background-color:rgb(210, 210, 210);
							}
			""")
		

		self.verticalHeader().hide()

		header_style = """::section{Background-color:rgb(150, 150, 150);
								  border-top-left-radius:10px;
								  border-top-right-radius:10px;
								  border-bottom-left-radius:0px;
								  border-bottom-right-radius:0px;
								  font-size:15px;
								  height:30px;}"""
		self.horizontalHeader().setStyleSheet(header_style)
 
		self.setGridStyle(Qt.NoPen)
		
		self.currentCellChanged.connect(self.selectionChange)
		self.parent = parent
		self.id = id_
		self.resizeRowsToContents()

		self.last_drop_row = None
		self.selectedRow = 0
		self.maxWidth = 0
	
	# Launch modification of an issue
	def modifyTodo_launch(self, event, widget):
		try :
			self.controller.modify_todo_launch(widget.uid)
		except Exception as e :
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText(str(e))
			msg.setStandardButtons(QMessageBox.Ok)
			msg.exec_()

	# Launch add of an issue
	def addTodo_launch(self):
		self.parent.addTodo_launch()

	# Launch close of an issue
	def closeTodo(self, event, widget):
		try :
			todo_row = self.selectedRow
			if self.controller.close_todo(widget.uid) :
				self.removeRow(todo_row)
				#open success popup
				self.controller.inform("This issue has been deleted.")
		except Exception as e :
			msg = QMessageBox()
			msg.setWindowTitle("Error")
			msg.setIcon(QMessageBox.Critical)
			msg.setText(str(e))
			msg.setStandardButtons(QMessageBox.Ok)
			msg.exec_()

	# Override this method to get the correct row index for insertion
	def dropMimeData(self, row, col, mimeData, action):
		self.last_drop_row = row
		return True

	# Method called when an issue is dropped in the card list
	def dropEvent(self, event):
		# get the source table from wich come the dropped widget
		sourceTable = event.source()

		# Default dropEvent method fires dropMimeData with appropriate parameters (we're interested in the row index).
		super(CardList, self).dropEvent(event)

		# Now we know where to insert selected row
		dropRow = self.last_drop_row

		# Get selected row
		selectedRow = sourceTable.getselectedRow()

		# if sourceTable == self, after creating new empty row selected row might change their locations
		if (self == sourceTable and selectedRow >= dropRow):
			selectedRow += 1

		# drop invalid
		if (selectedRow == -1):
			event.accept()
			return

		# TODO : Call controller status modification for issue

		# Allocate space for transfer
		self.insertRow(dropRow)

		# copy content of selected Widget into empty ones
		card = sourceTable.cellWidget(selectedRow, 0)
		if card:
			self.parent.changeTodoStatus(card.clone(), self, dropRow)

		# close selected row
		sourceTable.removeRow(selectedRow)

		# selecte new row
		self.setCurrentCell(dropRow, 0)
		
		# clear selection of other table
		self.parent.clearSelectionOther(self)

		event.accept()

	# Called when selection change
	def selectionChange(self, i, j, i2, j2):
		#Todo select on widget
		self.selectedRow = i
		
		if (i == -1):
			return

		#clear selection of other table
		self.parent.clearSelectionOther(self)

	# Return current row selected
	def getselectedRow(self):
		return self.selectedRow

	# Set cell (i,j) to given widget
	def setCellWidget(self, i, j, widget):
		super(CardList, self).setCellWidget(i, j, widget)
		widget.setList(self)

		# Update Card List size if needed
		self.maxWidth = max(widget.sizeHint().width(), self.maxWidth)
		self.setRowHeight(i, widget.sizeHint().height() + CardWidget.offset*2)
		self.setColumnWidth(0, self.maxWidth)
		left_margin = 3
		if self.verticalScrollBar().maximum == self.horizontalScrollBar().minimum :
			left_margin += CardWidget.offset*2 
		self.setFixedWidth(self.maxWidth + left_margin)

	# Automatic scroll to selected card
	def scrollToCard(self, card):
		self.setCurrentCell(self.rowAt(card.y()), 0)
		


class CardWidget(QWidget):
	selected = None
	offset = 8


	def __init__(self, todo, board):
		super(CardWidget, self).__init__()
		self.board = board
		self.list = None

		self.text = QLabel("", self)
		self.text.setWordWrap(True)

		#TODO change size based on window and status
		self.update(todo)

		self.setMinimumSize(10, self.sizeHint().height())
		
		self.resize(self.sizeHint())

		self.text.move(self.offset, self.offset)

	# Update card info from todo argument
	def update(self, todo):
		self.name = todo["summary"]
		self.text.setText(self.name)

		self.priority = todo.get("priority", None)
		if self.priority != None :
			self.color = PRIORITY_COLORS[min(self.priority - 1, 5)]
		else :
			self.color = PRIORITY_COLORS[0]

		self.uid = todo["uid"]
		
		self.resize(self.sizeHint())

	# Add right click menu
	def contextMenuEvent(self, event):
		self.menu = QMenu(self)

		# Add 'Create card' action
		createAction = QAction('Create card', self)
		createAction.triggered.connect(self.list.addTodo_launch)
		self.menu.addAction(createAction)

		# Add Modify action
		modifyAction = QAction('Modify', self)
		modifyAction.triggered.connect(lambda: self.list.modifyTodo_launch(event, self))
		self.menu.addAction(modifyAction)

		# Add Close action
		closeAction = QAction('Close', self)
		closeAction.triggered.connect(lambda: self.list.closeTodo(event, self))
		self.menu.addAction(closeAction)

		# Pop it up
		self.menu.popup(QCursor.pos())

	# Draw card object
	def paintEvent(self, e):
		width = self.width() - 3
		height = self.height() - 3

		qp = QPainter()
		qp.begin(self)

		# Set color, priority color
		qp.setBrush(self.color)
		
		# Draw rectangle
		qp.drawRect(2, 2, width, height)

		# If selected add little yellow circle at left top
		if CardWidget.selected is self :
			qp.setBrush(QColor(255, 225, 50))
			qp.drawEllipse(0, 0, 10, 10)

		qp.end()
	
	# Set self as selected CardWidget and redraw it
	def select(self):
		if CardWidget.selected == self:
			return

		if (CardWidget.selected != None) :
			CardWidget.selected.unselect()

		CardWidget.selected = self

		self.repaint()

	# Set which list this card is from
	def setList(self, list):
		self.list = list

	# Unselect card widget
	def unselect(self):
		CardWidget.selected = None
		self.repaint()

	# Create identic CardWidget
	def clone(self):
		todo = {"priority":self.priority, 
				"summary":self.name, 
				"uid":self.uid}
		return CardWidget(todo, self.board)

	# Return potential size of widget
	def sizeHint(self):
		size = self.text.sizeHint()
		size.setHeight(size.height() + self.offset*2)
		size.setWidth(size.width() + self.offset*2)
		return size

	# Method called when clicking to card
	def mousePressEvent(self, event):
		super(CardWidget, self).mousePressEvent(event)
		self.board.todo_clicked.emit(self.name)
		self.select()



class ModifyForm(QWidget):
	"""Widget for modification form"""
	def __init__(self, status, todo, controller):
		super(ModifyForm, self).__init__()
		self.form = QFormLayout()

		self.todo = todo

		#Add Summary
		self.form.addRow("Summary", QLineEdit(todo["summary"]))

		#Add Status
		status_combo = QComboBox()
		status_combo.addItems(status)
		status_combo.setCurrentIndex(status.index(todo["status"]))
		self.form.addRow("Status", status_combo)

		#Add Priority
		priority_combo = QComboBox()
		priority_combo.addItems([str(x) for x in range(1, 7)])
		priority_combo.setCurrentIndex(todo["priority"]-1)
		self.form.addRow("Priority", priority_combo)

		if todo.get("dtstart", False):
			#Add Dtstart
			dtstart_calendar = QCalendarWidget()
			dtstart_calendar.setSelectedDate(todo["dtstart"].dt)
			self.form.addRow("Dtstart", dtstart_calendar)

		self.textError = QLabel("")

		hlayout = QHBoxLayout()

		# Create Modify (confirmation) button
		modify_button = QPushButton("Modify")
		modify_button.clicked.connect(controller.modify_todo_content)
		# Create Cancel (abort) button
		cancel_button = QPushButton("Cancel")
		cancel_button.clicked.connect(self.hide)

		# Add button to layout
		hlayout.addWidget(modify_button)
		hlayout.addWidget(cancel_button)

		#Wrap all to main layout
		vlayout = QVBoxLayout()
		vlayout.addWidget(self.textError)
		vlayout.addLayout(self.form)
		vlayout.addLayout(hlayout)

		self.setLayout(vlayout)


	def display_error(self, exception):
		print('Error : '+str(exception))
		self.textError.setText("<font color='red'>" + str(exception) + "</font>")


	def getArgs(self):
		args = {}
		for i in range(self.form.rowCount()):
			key = str(self.form.itemAt(i, 0).widget().text()).lower()
			value = self.form.itemAt(i, 1).widget()

			#TODO : add a more flexible way to convert qt to value
			#SUGGESTION : rewrite all fields class with a method "getValue" and simplify all getArgs method
			if type(value) == QLineEdit :
				value = str(value.text())
			elif type(value) ==  QComboBox :
				value = str(value.currentText())
			elif type(value) == QCalendarWidget :
				value = str(value.selectedDate().toString(format=Qt.ISODate))
				value = date_parse(value)
				value = vDDDTypes(value)
			args[key] = value
		return args


class AddForm(QWidget):
	"""Widget for add card form"""
	def __init__(self, status, config, controller):
		super(AddForm, self).__init__()
		self.form = QFormLayout()

		#Add Summary
		self.form.addRow("Summary", QLineEdit())

		#Add Status
		status_combo = QComboBox()
		status_combo.addItems(status)
		self.form.addRow("Status", status_combo)

		#Add Priority
		priority_combo = QComboBox()
		priority_combo.addItems([str(x) for x in range(1, 7)])
		self.form.addRow("Priority", priority_combo)

		#Add all model property
		for item, ItemClass in config.items() :
			self.form.addRow(item.capitalize(), ItemClass())

		self.textError = QLabel("")

		hlayout = QHBoxLayout()

		# Create Add (confirmation) button
		add_button = QPushButton("Create issue")
		add_button.clicked.connect(controller.add_todo_content)
		# Create Cancel (abort) button
		cancel_button = QPushButton("Cancel")
		cancel_button.clicked.connect(self.hide)

		# Add button to layout
		hlayout.addWidget(add_button)
		hlayout.addWidget(cancel_button)

		#Wrap all to main layout
		vlayout = QVBoxLayout()
		vlayout.addWidget(self.textError)
		vlayout.addLayout(self.form)
		vlayout.addLayout(hlayout)

		self.setLayout(vlayout)


	def display_error(self, exception):
		print('Error : '+str(exception))
		self.textError.setText("<font color='red'>" + str(exception) + "</font>")


	def getArgs(self):
		args = {}
		for i in range(self.form.rowCount()):
			key = str(self.form.itemAt(i, 0).widget().text()).lower()
			value = self.form.itemAt(i, 1).widget()

			#TODO : add a more flexible way to convert qt to value
			#SUGGESTION : rewrite all fields class with a method "getValue" and simplify all getArgs method
			if type(value) == QLineEdit :
				value = str(value.text())
			elif type(value) ==  QComboBox :
				value = str(value.currentText())
			elif type(value) == QCalendarWidget :
				value = str(value.selectedDate().toString(format=Qt.ISODate))
				value = date_parse(value)
				value = vDDDTypes(value)
			args[key] = value
		return args

	

class QPasswordLineEdit(QLineEdit):
	"""QLineEdit with field as Password with *** character"""
	def __init__(self):
		super(QPasswordLineEdit, self).__init__()
		self.setEchoMode(QLineEdit.Password)



def test_Little_Bugzilla(controller):
	#TODO : search a static example
	controller.load_model({"model_type" : "BugZilla"})
	args = {"url" : "https://bugzilla.mozilla.org/buglist.cgi?component=AutoConfig%20%28Mission%20Control%20Desktop%29&product=Core&query_format=advanced&resolution=---&ctype=ics"}
	controller.get_calendar(args)


def test_Big_Bugzilla(controller):
	#TODO : search a static example
	controller.load_model({"model_type" : "BugZilla"})
	args = {"url" : "https://bugzilla.mozilla.org/buglist.cgi?component=Canvas%3A%202D&limit=0&product=Core&query_format=advanced&resolution=---&ctype=ics"}
	controller.get_calendar(args)


def test_demo_Redmine(controller):
	# TODO : set a static example
	controller.load_model({"model_type" : "Redmine API", "url":"http://demo.redminecrm.com", "username":"admin", "password":"admin"})
	args = {"identifier":"agile"}
	controller.get_calendar(args)

def test_CalDav(controller):
	# TODO : set example
	#"""
	print("test CalDav not set yet")
	controller.quit()
	"""
	# EXAMPLE
	controller.load_model({"model_type":"CalDav Server", "url":"http://url.com/caldav.php",
									 "username":"user",
									 "password":"pass"})
	args = {"name" : "calendar display name"}
	controller.get_calendar(args)#"""



if __name__ == '__main__':
	from controller import *
	c = Controller()
	test_list = [test_Little_Bugzilla, test_Big_Bugzilla, test_demo_Redmine]
	print("Choose a test : ")
	for i in range(len(test_list)) :
		print("\t(" + str(i).replace("_", " ") + ") " + test_list[i].__name__)
	test_index = int(input("Enter test id (0, 1 ...etc) : "))
	test_list[test_index](c)
	sys.exit(c.app.exec_())
	